# Planning

## Research

* [ ] Vocab
* [ ] Functions
* [ ] Methods

## Problem decomposition

* [ ] Input
* [ ] Output
* [ ] Examples
* [ ] Conditions (if)
* [ ] Iteration (loop)

## Problems

### 01 minimum_value

Add plans...

This takes two arguments which are two values. You evaluate which is greater with the `>` or `<` operator.
This is a simple comparison so you'd write an if statement that would return whichever value is smaller. 

If they have the same value, you return either value and it will be correct.


### 04 max_of_three

Given 3 values, you must find the one with the greatest value. You must check at least two values and compare them with the other two to see if it is greater. If the first two aren't, then the last one must be. If they're equal then it doesn't matter which is returned.

### 06 can_skydive

Determines the eligibility of a would-be skydiver. You must verify first if they at least 18 years old. Then you must check if they have a signed consent form. 

if age >= 18 and consent form is signed then they can skydive. Otherwise they cannot.
 
### 09 is_palindrome

Given a word, you would copy the word into a second variable, reverse it, and perform a comparison. If it is the same with reversed characters, then it is a palindrome. else it is not.

### 10 is_divisible_by_3

The only condition that something would be divisible by 3 is if modulo 3 returns 0, so you would evaluate this by performing modulo on the given number

so 9%3 = 0 so it is 
10%3 = 1 so it is not
11%3 = 2 so it is not

### 11 is_divisible_by_5

You would use an identical approach as the previous question, but with 5


### 12 fizzbuzz
  You are checking if a number is various cases. Just make sure that you are following the correct order. checking for both being true needs to be the first step when writing out your conditionals.

### 14 can_make_pasta

all() function can be used to compare lists and return its truthyness. 


### 16 is_inside_bounds

The constraints of the bounds are they are greater than 0 and less than 10 in both x and y axes. The default condition is false. 

### 19 is_inside_bounds

You're checking if a rectange is inside the bounds of a given constraint of x and y. In order for it to be within the constraints given, it needs its origin coordinates of rect_x and rect_y to be within those boundaries. It also needs to chek if the rec_x + rect_width is still smaller than x and rect_y + rect_height is still less than y.

If any of those conditions aren't met it is false.


### 20 has_quorum

In order to solve this, given the list of attendees and the members list, you are taking the count of the members list and comparing it with the attendees list. If the attendees number is greater than 50% then you have quorum.

so let's say there are 100 members. if there are 50 attendees, you would have quorum. In other words, you would divide the members list by 2 and check to see if the attendees list is equal or greater to that number. If it is an odd number, you should round it and compare.

### 22 gear_for_day

The only tricky part is that you only have an umbrella if it's both sunny and a workday. Everything is simple conditionals and you run through every single one.

### 24 calculate_average

You will take the given list, add together the value of each entry and divide it by the total number of entries. If it's empty then return None

### 25 calculate_sums

Iterate through list and add each value as you go in a separate variable. Return variable after iterating.

### 26 calculate_grade


You are doing the same as in previous calculate_average but then applying an additional conditional for what letter grade to assign to it.

### 27 max_in_list

Iterate through the list. Check if the previous highest number, which can be set to the first item on the list on initialization, is greater than it. If it is, replace it with that number. Otherwise, keep old number and continue iteration.


### 28 remove_duplicate_letters

you  accomplish this by iterating through the string. In a new "result" string, you add a character if it is not currently in the string. If it is, you skip adding it. 

YOu then return the string at the end.

### 30 find_second_largest

If you're trying to find the second largest sum in a list, it means you have to keep track of two values, the current highest, and the second highest. 

When you initialize the variables, you can compare the first two items, put the highest in the "largest", smaller in "second_largest." Then you will iterate through the rest of the array. If it is the highest number, then the curren thighest number gets reassigned to second_highest and that number becomes the highest. After you iterate through the list, you return the second_highest

### 31 sum_of_squares

First you would iterate through the values and square each of them. You'd then iterate through them and add them all together, storing the sum in a separate variable. return that sum.

### 32 sum_of_first_n_numbers

You need to create a list with all the numbers in the limit. You then use that to iterate through each number and add the sum of all the list elements.

### 33 sum_of_first_n_even_numbers

Similar, except that you add each consecutive even number up from the limit. If you put '5', you want 0, 2, 4, 6, 8, and 10. The first 6 would be 0, 2, 4, 6 8, 10, and 12. So in order to create the list, you would take the given number, double it, then iterate through that, while only adding even numbers

### 34 count_letters_and_digits

You are keeping track of two sums, the sum of all the letters and a sum of all the numbers, ultimately you return both. You check this with built-in functions as you iterate through the list

### 37 pad_left

You first need to figure out how many digits are in a given number. Probably the easiest way is to convert it to a string and then get the count of the given number. so With the example '10', you'd get its length by `len(str(num))` you take this, subtract it from the pad length, then iterate through an array of that length, add the pad to the string, then finally append the number to the end of the string.

### 38 reverse_dictionary

Python allows you to swap the key and value as you're iterating through the dictionary.

### 41 add_csv_lines

split strings into individual numbers, convert to numbers, add them together, return the sum

### 42 pairwise_add

Given that the input is always two lists of the same length, you may iterate using the length of one, adding the two, and appending i to the new list

### 43 find_indexes

`enumerate()` function in python will add numbers and add them in the list in CSV


### 44 translate

This is to convert a dictionary into a list. You use the `get()` to pull the values form the dictionary by the keys values provided.


### 46 make_sentences

no pseudocode

### 47  check_password

regex to the rescue

### 48 count_word_frequencies


### 49 sum_two_numbers

return x + y

### 50 halve_the_list

you figure out the cutoff point, iterate through the numbers, place numbers up to the cutoff in the first half, the rest in the second half. return both of those

### 51 safe_divide

just a check and use math.inf

### 52 generate_lottery_numbers

iterate through a for loop as many times as you want unique numbers. Append a random number that is unique each time. You can do this recursively.

### 53 username_from_email

regex used here as well.

### 54 check_input

if it says "raise" then raise ValueError()

### 56 num_concat

convert the numbers to strings then return the addition of the two

### 58 group_cities_by_state

You would need to create a new dictionary whose key values are the state. You would split the input into a list.

### 59 specific_random

itrearate through all numbers, check for remainders of 0 to add to list for both 7 and 5

### 60 only_odds

create empty list. iterate through the numbers list, take number % 2 and if it is 1 then it is odd. append to list. return 

### 61 remove_duplicate

empty list, iterate through original list. check if that value is in the new list. if it isn't add it, other wise iterate. return at the end
