import math
# Write a function that meets these requirements.
#
# Name:       halve_the_list
# Parameters: a single list
# Returns:    two lists, each containing half of the original list
#             if the original list has an odd number of items, then
#             the extra item is in the first list
#
# Examples:
#    * input: [1, 2, 3, 4]
#      result: [1, 2], [3, 4]
#    * input: [1, 2, 3]
#      result: [1, 2], [3]

def halve_the_list(numbers):
    half1 =[]
    half2 =[]
    half_cutoff = math.ceil(len(numbers) /2)
    print(half_cutoff)
    for num in range(len(numbers)):
        if num < half_cutoff:
            half1.append(numbers[num])
        else:
            half2.append(numbers[num])
    return half1, half2

input= [1, 2, 3, 4]
print(halve_the_list(input))
#      result: [1, 2], [3, 4]
input= [1, 2, 3]
print(halve_the_list(input))
#      result: [1, 2], [3]
