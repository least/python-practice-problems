# Complete the can_make_pasta function to
# * Return true if the ingredients list contains
#   "flour", "eggs", and "oil"
# * Otherwise, return false
#
# The ingredients list will always contain three items.

# Do some planning in ./planning.md

# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

def can_make_pasta(ingredients):
    required_ingredients = ["eggs", "flour", "oil"]

    return all(ingredient in ingredients for ingredient in required_ingredients) # returns true only if every ingredient in required_ingredients is in the ingredients list

test1 = ["flour", "eggs", "oil"] # True
test2 = ["juice", "eggs", "oil"] # False
test3 = ["flour", "eggs", "milk"]# False

print(can_make_pasta(test1))
print(can_make_pasta(test2))
print(can_make_pasta(test3))
