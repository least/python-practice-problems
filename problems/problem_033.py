# Complete the sum_of_first_n_even_even_numbers function which
# accepts a numerical count n and returns the sum of the
# first n even numbers
#
# If the value of the limit is less than 0, then it should
# return None
#
# Examples:
#   * -1 returns None
#   * 0 returns 0
#   * 1 returns 0+2=2
#   * 2 returns 0+2+4=6
#   * 5 returns 0+2+4+6+8+10=30
#
# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

def sum_of_first_n_even_numbers(n):
    if n < 0:
        return None
    elif n == 0:
        return 0
    sum = 0
    numbers = []
    double = n * 2
    for num in range(double+2):
        if num % 2 == 0:
            numbers.append(num)
    # print(numbers)
    for num in numbers:
        sum += num
    return sum


#   * -1 returns None
print(sum_of_first_n_even_numbers(-1))# None
#   * 0 returns 0
print(sum_of_first_n_even_numbers(0)) # 
#   * 1 returns 0+2=2
print(sum_of_first_n_even_numbers(1)) # 
#   * 2 returns 0+2+4=6
print(sum_of_first_n_even_numbers(2)) # 
#   * 5 returns 0+2+4+6+8+10=30
print(sum_of_first_n_even_numbers(5)) #
    
print(sum_of_first_n_even_numbers(3)) # 6
print(sum_of_first_n_even_numbers(4)) # 10
