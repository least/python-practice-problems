# Complete the max_in_list function to find the
# maximum value in a list
#
# If the list is empty, then return None.
#

def max_in_list(values):
    if not values: 
        return None
    max_value = values[0] # start with the first item in the list
    for i in range(len(values)):
        if max_value < values[i]:
            max_value = values[i]
    return max_value


values = [1, 2, 3, 4, 5]
print(max_in_list(values)) # 5
values = [10, 2, 3, 4, 5]
print(max_in_list(values)) # 10
values = [-1, -2, -3, -4, 5]
print(max_in_list(values)) # -1
values = [10, 200, 3028, 4, -5]
print(max_in_list(values)) # 3028
