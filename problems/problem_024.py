# Complete the calculate_average function which accepts
# a list of numerical values and returns the average of
# the numbers.
#
# If the list of values is empty, the function should
# return None
#
# Pseudocode is available for you

def calculate_average(values):
    if not values: #empty list is falsy
        return None
    divisor = len(values)
    total = 0

    for value in values:
        total += value
    return total / divisor

values = [1, 2, 3, 4, 5]
print(calculate_average(values)) # returns 3

values = [1, 2, 3, 4]
print(calculate_average(values)) # returns 2.5

values = []
print(calculate_average(values)) # returns None
