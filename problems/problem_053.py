# Write a function that meets these requirements.
#
# Name:       username_from_email
# Parameters: a valid email address as a string
# Returns:    the username portion of the email address
#
# The username portion of an email is the substring
# of the email address that appears before the @
#
# Examples
#    * input:   "basia@yahoo.com"
#      returns: "basia"
#    * input:   "basia.farid@yahoo.com"
#      returns: "basia.farid"
#    * input:   "basia_farid+test@yahoo.com"
#      returns: "basia_farid+test"

import re
# Assume that the email address is a valid one.
def username_from_email(email):
    regex = r"^.*?(?=@)"

    # ^ is the start of the string. This tells the regex that the match must be starting from the beginning of the string. 
    # . is to match any character
    # * matches all of that '.' up to
    # ? which means it only matches the first occurrence. without it, you could match a later occurrence of the @. If you search for "basia.farid+teset@yahoo.com@hotmail.com" without the ?, it returns "basia.farid+test@yahoo.com". This may or may not be what you want. 
    # (?=@) () is for the capture group, which excludes it from the rest of the match. #?= is positive look ahead so it looks for it throughout the entire string. the @ is to look for the @ character. It being in its own capture group excludes it from the search group that is a match. Without this, it includes the @ in the result.
    username = re.search(regex, email).group()
    return username

print(username_from_email("dude@dude.com"))
print(username_from_email("basia@yahoo.com"))
print(username_from_email("basia.farid@yahoo.com"))
print(username_from_email("basia.farid+test@yahoo.com"))
print(username_from_email("basia.farid+test@yahoo.com@hotmail.com"))
