# Complete the is_inside_bounds function which has the
# following parameters:
#   x: the x coordinate to check
#   y: the y coordinate to check
#   rect_x: The left of the rectangle
#   rect_y: The bottom of the rectangle
#   rect_width: The width of the rectangle
#   rect_height: The height of the rectangle
#
# The is_inside_bounds function returns true if all of
# the following are true
#   * x is greater than or equal to rect_x
#   * y is greater than or equal to rect_y
#   * x is less than or equal to rect_x + rect_width
#   * y is less than or equal to rect_y + rect_height

def is_inside_bounds(x, y, rect_x, rect_y, rect_width, rect_height):
    rect_right_boundary = rect_x + rect_width
    rect_upper_boundary = rect_y + rect_height
    if rect_x >= 0 and rect_right_boundary <= x and rect_y >= 0 and rect_upper_boundary <= y:
        return True
    return False

print(is_inside_bounds(10, 10, 1, 1, 3, 3)) # True
print(is_inside_bounds(3, 3, 1, 1, 3, 3)) # False 
print(is_inside_bounds(-3, 3, 1, 1, 3, 3)) # False 
print(is_inside_bounds(3, 3, 4, 5, 3, 3)) # False 
