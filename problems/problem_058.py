# Write a function that meets these requirements.
#
# Name:       group_cities_by_state
# Parameters: a list of cities in the format "«name», «st»"
#             where «name» is the name of the city, followed
#             by a comma and a space, then the two-letter
#             abbreviation of the state
# Returns:    a dictionary whose keys are the two letter
#             abbreviations of the states in the list and
#             whose values are a list of the cities appearing
#             in that list for that state
#
# In the items in the input, there will only be one comma.
#
# Examples:
#     * input:   ["San Antonio, TX"]
#       returns: {"TX": ["San Antonio"]}
#     * input:   ["Springfield, MA", "Boston, MA"]
#       returns: {"MA": ["Springfield", "Boston"]}
#     * input:   ["Cleveland, OH", "Columbus, OH", "Chicago, IL"]
#       returns: {"OH": ["Cleveland", "Columbus"], "IL": ["Chicago"]}
#
# You may want to look up the ".strip()" method for the string.

def group_cities_by_state(names_and_states):
    new_dict = {}
    for city_state in names_and_states:
        split_list = city_state.split(",")
        city = str(split_list[0])
        state = ''.join(split_list[1].split())
        if not state in new_dict:
            new_dict[state] = [city]
        else:
            new_dict[state].append(city)


    return new_dict

names_and_states=   ["San Antonio, TX"]
print(group_cities_by_state(names_and_states))
#       returns: {"TX": ["San Antonio"]}
names_and_states=   ["Springfield, MA", "Boston, MA"]
print(group_cities_by_state(names_and_states))
#       returns: {"MA": ["Springfield", "Boston"]}
names_and_states=   ["Cleveland, OH", "Columbus, OH", "Chicago, IL"]
print(group_cities_by_state(names_and_states))
#       returns: {"OH": ["Cleveland", "Columbus"], "IL": ["Chicago"]}
