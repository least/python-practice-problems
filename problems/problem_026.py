# Complete the calculate_grade function which accepts
# a list of numerical scores each between 0 and 100.
#
# Based on the average of the scores, the function
# returns
#   * An "A" for an average greater than or equal to 90
#   * A "B" for an average greater than or equal to 80
#     and less than 90
#   * A "C" for an average greater than or equal to 70
#     and less than 80
#   * A "D" for an average greater than or equal to 60
#     and less than 70
#   * An "F" for any other average

def calculate_grade(values):
    divisor = len(values)
    total = 0

    for value in values:
        total += value

    average = total / divisor

    match average:
        case _ if average >= 90: 
            return "A"
        case _ if average >= 80: 
            return "B"
        case _ if average >= 70: 
            return "C"
        case _ if average >= 60: 
            return "D"
        case _:
            return "F"

values =[ 100, 99, 88, 92]
print(calculate_grade(values)) # A 94.75
values =[ 80, 89, 88, 92]
print(calculate_grade(values)) # B 87.25 
values =[ 60, 89, 78, 72]
print(calculate_grade(values)) # C 74.75
values =[ 60, 59, 38, 92]
print(calculate_grade(values)) # D 62.25
values =[ 60, 59, 38, 22]
print(calculate_grade(values)) # F 44.75
