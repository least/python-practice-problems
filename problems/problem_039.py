# Complete the reverse_dictionary function which has a
# single parameter that is a dictionary. Return a new
# dictionary that has the original dictionary's values
# for its keys, and the original dictionary's keys for
# its values.
#
# Examples:
#   * input:  {}
#     output: {}
#   * input:  {"key": "value"}
#     output: {"value", "key"}
#   * input:  {"one": 1, "two": 2, "three": 3}
#     output: {1: "one", 2: "two", 3: "three"}

def reverse_dictionary(dictionary):
    print(dictionary)
    return {value:key for key, value in dictionary.items()}
# value:key for key, value being inside {} means that the key becomes the value and the value becomes the key as it iterates across dictionary.items(), which returns the original key and value of the dictionary. 

input=  {}
print(reverse_dictionary(input))
#     output: {}
input=  {"key": "value"}
print(reverse_dictionary(input))
#     output: {"value", "key"}
input=  {"one": 1, "two": 2, "three": 3}
print(reverse_dictionary(input))
#     output: {1: "one", 2: "two", 3: "three"}


