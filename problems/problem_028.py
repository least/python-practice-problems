# Complete the remove_duplicate_letters that takes a string
# parameter "s" and returns a string with all of the
# duplicates removed.
#
# Examples:
#   * For "abc", the result is "abc"
#   * For "abcabc", the result is "abc"
#   * For "abccba", the result is "abc"
#   * For "abccbad", the result is "abcd"
#
# If the list is empty, then return the empty string.

def remove_duplicate_letters(s):
    if not s:
        return ""
    result = "" 
    for char in s:
        if char not in result:
            result = result + char

    return result

s = "abc"
print(remove_duplicate_letters(s))
#   * For "abcabc", the result is "abc"
s =  "abcabc"
print(remove_duplicate_letters(s))
#   * For "abccba", the result is "abc"
s =  "abccba"
print(remove_duplicate_letters(s))
#   * For "abccbad", the result is "abcd"
s =  "abccbad"
print(remove_duplicate_letters(s))
