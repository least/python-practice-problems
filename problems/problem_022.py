# Complete the gear_for_day function which returns a list of
# gear needed for the day given certain criteria.
#   * If the day is not sunny and it is a workday, the list
#     needs to contain "umbrella"
#   * If it is a workday, the list needs to contain "laptop"
#   * If it is not a workday, the list needs to contain
#     "surfboard"

def gear_for_day(is_workday, is_sunny):
    equipment_list = []
    if is_workday:
        equipment_list.append("laptop")
    if not is_workday:
        equipment_list.append("surfboard")
    if not is_sunny and is_workday:
        equipment_list.append("umbrella")
    return equipment_list

print(gear_for_day(True, True)) # laptop
print(gear_for_day(False, True)) # "surfboard"
print(gear_for_day(True, False)) # laptop, umbrella
print(gear_for_day(False, False)) # "surfboard"

