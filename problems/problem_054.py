# Write a function that meets these requirements.
#
# Name:       check_input
# Parameters: one parameter that can hold any value
# Returns:    if the value of the parameter is the
#             string "raise", then it should raise
#             a ValueError. otherwise, it should
#             just return the value of the parameter
#
# Examples
#    * input:   3
#      returns: 3
#    * input:   "this is a string"
#      returns: "this is a string"
#    * input:   "raise"
#      RAISES:  ValueError


def check_input(value):
    if value == "raise":
        raise ValueError()
    return value

input=   3
print(check_input(input))
#      returns: 3
input=   "this is a string"
print(check_input(input))
#      returns: "this is a string"
input=   "raise"
print(check_input(input))
#      RAISES:  ValueError
