# Complete the has_quorum function to test to see if the number
# of the people in the attendees list is greater than or equal
# to 50% of the number of people in the members list.

def has_quorum(attendees_list, members_list):
    attendees_num = len(attendees_list)
    members_num = len(members_list)
    quorum_target = members_num / 2

    if attendees_num >= quorum_target:
        return True
    return False


members_list= ['Jane', 'Joe', 'Jerry', 'Josh', "Janet"]
attendees_list= [ 'Jerry', 'Josh', "Janet"]

print(has_quorum(attendees_list, members_list)) # True


members_list= ['Jane', 'Joe', 'Jerry', 'Josh', "Janet"]
attendees_list= [ 'Josh', "Janet"]
print(has_quorum(attendees_list, members_list)) #  False


members_list= ['Jane', 'Joe', 'Josh', "Janet"]
attendees_list= [ 'Josh', "Janet"]
print(has_quorum(attendees_list, members_list)) #  True

