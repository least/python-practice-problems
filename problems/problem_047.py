import re
# Complete the check_password function that accepts a
# single parameter, the password to check.
#
# A password is valid if it meets all of these criteria
#   * It must have at least one lowercase letter (a-z)
#   * It must have at least one uppercase letter (A-Z)
#   * It must have at least one digit (0-9)
#   * It must have at least one special character $, !, or @
#   * It must have six or more characters in it
#   * It must have twelve or fewer characters in it
#
# The string object has some methods that you may want to use,
# like ".isalpha", ".isdigit", ".isupper", and ".islower"

def check_password(password):
    # ^ is the start of the string, $ is the end of the string
    # Parenthesis creates capture groups so you can extract specific things from the string you're parsing
    # ?=.* this is a "positive look ahead" that matches any character. This is so each capture group looks at every character in the string, regardless of its order in the capture group.check_password 
    # If this wasn't included, it'd only be a valid password in the order of the specifications. In this case, upper cases => lower case => digits => special characters... which would make many passwords invalid
    # [A-Z] Checks to see if there is an instance of an uppercase letter
    # [a-z] Checks to see if there is an instance of an lowercase letter
    # \d  checks for digits
    # [$!@] checks for the existence of the three specified special characters of "$", "!", or "@"
    # \S{6,12} makes sure that the string is between 6 and 12 characters and contains no whitespace
    # $ marks the end of the string
    regex = r"^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[$!@])\S{6,12}$"
    is_valid = re.search(regex, password)
    if not is_valid: # search returns a Match object if there is a match. Otherwise it returns None, which is falsy
        return False
    return True

print(check_password("1Ba")) # False
print(check_password("$1ABcde")) # True
print(check_password("aB8!23")) # True
print(check_password("aB8!*23")) # True
print(check_password("$1ABcde8d8hgbklsdl")) # False
