# Write a function that meets these requirements.
#
# Name:       shift_letters
# Parameters: a string containing a single word
# Returns:    a new string with all letters replaced
#             by the next letter in the alphabet
#
# If the letter "Z" or "z" appear in the string, then
# they would get replaced by "A" or "a", respectively.
#
# Examples:
#     * inputs:  "import"
#       result:  "jnqpsu"
#     * inputs:  "ABBA"
#       result:  "BCCB"
#     * inputs:  "Kala"
#       result:  "Lbmb"
#     * inputs:  "zap"
#       result:  "abq"
#
# You may want to look at the built-in Python functions
# "ord" and "chr" for this problem

# print(ord("A"))
# print(ord("a"))
# print(ord("Z"))
# print(ord("z"))
def shift_letters(word):
    shifted_word = ""
    for char in word:
        unicode_char = ord(char)
        if unicode_char == 90: # if the character is 'Z'
             shifted_word = shifted_word + chr(65) # 'A'
        elif unicode_char == 122: # if the character is 'z'
            shifted_word = shifted_word + chr(97) # 'a'
        else:
            shifted_word = shifted_word + chr(unicode_char+1)
            
    return shifted_word

word=  "import"
print(shift_letters(word))
# result:  "jnqpsu"
word=  "ABBA"
print(shift_letters(word))
# result:  "BCCB"
word=  "Kala"
print(shift_letters(word))
# result:  "Lbmb"
word=  "zap"
print(shift_letters(word))
# result:  "abq"
word=  "zZzbBb"
print(shift_letters(word))
# result:  "aAacCc"
