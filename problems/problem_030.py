# Complete the find_second_largest function which accepts
# a list of numerical values and returns the second largest
# in the list
#
# If the list of values is empty, the function should
# return None
#
# If the list of values has only one value, the function
# should return None
#
# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

def find_second_largest(values):
    if len(values) <= 1:
        return None
    if values[0] > values[1]:
        highest = values[0]
        second_highest = values[1]
    else:
        highest = values[1]
        second_highest = values[0]
    for value in values[2:]:
        if value > highest: 
            second_highest = highest
            highest = value
        elif value > second_highest:
            second_highest = value
    return second_highest

values =[]
print(find_second_largest(values)) # None
values =[1, 2, 3]
print(find_second_largest(values)) # 2

values =[-1, 2, -3]
print(find_second_largest(values)) # -1 

values =[1, 20, 13]
print(find_second_largest(values)) #  13

values =[1, 20]
print(find_second_largest(values)) #  1

values =[1]
print(find_second_largest(values)) #  None

values =[ 181, 20, 13, 200, 380, 150]
print(find_second_largest(values)) #  200
