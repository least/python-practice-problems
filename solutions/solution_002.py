# Complete the minimum_value function so that returns the
# minimum of two values.
#
# If the values are the same, return either.
#
# Do it without pseudocode, this time, from memory. Don't look
# at the last one you just wrote unless you really must.

def minimum_value(value1, value2):
    if value1 < value2:  # solution
        return value1    # solution
    return value2    # You can safely return value2 in both cases of it either being less than value1 or in the case it is the same as value1
    # pass               # problem
